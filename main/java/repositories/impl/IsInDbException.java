package repositories.impl;

public class IsInDbException extends Exception {
	
	  /**
	 * 
	 */
	private static final long serialVersionUID = 6741515073040664827L;

	public IsInDbException() {
		  System.out.println("It isn't in the database!");
		  }

}
