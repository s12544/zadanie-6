package repositories;

import java.util.List;

import domain.RolesPermissions;

public interface IRolesPermissionRepository extends IRepository<RolesPermissions> {
	
	public List<RolesPermissions> withRoleId(int roleId);
	public List<RolesPermissions> withPermissionId(int permissionId);

}
